import { GitHubActivityData, GitHubUserSearchData } from "./types";

import { Octokit } from '@octokit/rest';

const octokit = new Octokit();

export default class GitHubService {
    static readonly GIT_API_URL = 'https://api.github.com/';

    public static async getUserActivity(username: string): Promise<GitHubActivityData> { 
        try {
           const results = await octokit.rest.activity.listEventsForAuthenticatedUser({
               username: username,
             });

           return results.data;
      
        } catch (error: any) {
            console.log(error.message);
            throw new Error(`User ${error.message}`);
        }
    }

    public static async searchUsers(username: string, page?: number): Promise<GitHubUserSearchData>  { // add this : Promise<GitHubUserSearchData>
      try {
           const results = await octokit.rest.search.users({
              q: username,
            });
            return results.data;
        
      } catch (error: any) {
        console.log(error.message);
        throw new Error(error.message);
      }
    }
}