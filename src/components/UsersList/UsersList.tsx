import { Link } from "react-router-dom";

 const UsersList = (props: any) => {
  let users: any = [];
  for (let index = 0; index < props.name.length; index++) {
    users[index] = props.name[index];
  }

  return (
    <div className="search-results">
      <ul className="user-list-wrap">
        {users.map((item: any) => (
          <Link
            to={{
              pathname: `/user-activity/${item.login}`,
              state: item.login, 
            }}
            key={item.id}
          >
            <li className="user">
              <div className="avartar-wrap">
                <img src={`${item.avatar_url}`} alt="" />
              </div>
              <div className="username">
                <p>{item.login}</p>
              </div>
            </li>
          </Link>
        ))}
      </ul>
    </div>
  );
};

export default UsersList;
