import { useState } from "react";
import ActivityListItem from "./ActivityListItem";

const ActivityList = (props: any) => {

  let [isVisible, setIsVisible ] = useState(false)

  let [repo, setRepo] = useState();

  let users: any = [];
  for (let index = 0; index < props.activity.length; index++) {
    users[index] = props.activity[index];
  }

  function handleClick(item:any){
    console.log(item);
    repo = item.repo
    setRepo(repo)
     isVisible = !isVisible;
     setIsVisible(isVisible);
  }
  return (
    <div className="">
      <p className="tag-line">Repos</p>
      <p className="">{props.username}</p>
      <div className="search-results">
        <ul className="repos-wrap">
          <ActivityListItem showMore={isVisible} repo={repo} />
          {users.map((item: any) => (
            <li
              key={item.id}
              className="user"
              onClick={() => {
                handleClick(item);
              }}
            >
              <div className="avartar-wrap">
                <img src={`${item.org?.avatar_url}`} alt="" />
              </div>
              <div className="repo-title">
                <p>{item.repo.name}</p>
                <span className="timestamp">{item.created_at}</span>
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default ActivityList;
