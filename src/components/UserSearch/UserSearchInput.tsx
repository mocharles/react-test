import { useState, useEffect } from "react";
import { JsxEmit } from "typescript";
import GitHubService from "../../services/github/Github";
import UsersList from "../UsersList/UsersList";

const UserSearchInput = (props:any) => {
      const [searchInput, setSearchInput] = useState("");
      let [users, setUsers] = useState({});
    
      let response;

      const handleChange = (e: any) => {
        setSearchInput(e.target.value);
      };

      const getUserInput = async () => {
        response = await GitHubService.searchUsers(searchInput);
        localStorage.setItem("searchResults", JSON.stringify(response.items));
        setUsers(response.items);
      };

      const results = localStorage.getItem("searchResults");
      users = results ? JSON.parse(results) : [];
      
      
    return (
        <>
      <div className="search-wrap">
        <input
          type="text"
          placeholder="e.g mocharles"
          value={searchInput}
          onChange={handleChange}
        />
        <button className="btn search-btn" onClick={getUserInput}>
          Find
        </button>
      </div>
       <UsersList name={users} />
       </>
    );
};

export default UserSearchInput;