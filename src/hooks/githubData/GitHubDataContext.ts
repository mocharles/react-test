import { createContext } from 'react';

interface IGitHubDataContext {
}

export type GitHubDataContext = IGitHubDataContext;

export const gitHubDataContext = createContext<GitHubDataContext | undefined>(undefined);