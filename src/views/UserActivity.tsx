import GitHubService from "../services/github/Github";
import { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import ActivityList from "../components/ActivityList/ActivityList";

const UserActivityView = () => {
  let [useractivity, setUserActivity] = useState({});
  const location = useLocation();
  const username = location.pathname.split("/")[2];

  const getUserInput = async () => {
    const response = GitHubService.getUserActivity(username);
    return await response.then((results: any) => {
      useractivity = results;
      setUserActivity(useractivity);
    })
  };

  useEffect(() => {getUserInput();}, []);

  return (
    <div>
      <ActivityList username={username} activity={useractivity} />
    </div>
  );
};

export default UserActivityView;
