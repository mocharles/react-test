import { useState } from "react";
import GitHubService from "../services/github/Github";
import UsersList from "../components/UsersList/UsersList";
import UserSearchInput from "../components/UserSearch/UserSearchInput";

const UserSearchView = () => {

  return (
    <div className="wrapper">
        <h1 className="tag-line">Find a Github User</h1>
        <p className="helper-text">
          Search users on Github by their username or a partial of their
          username
        </p>
        <UserSearchInput />
    </div>
  );
};

export default UserSearchView;
